﻿namespace GO
{
    public enum GoStone
    {
        Clear = 0, // no stone
        Black = 1, // black stone
        White = 2 // white stone
    }
}