# Signal GO

* [Signal GO at Azure](http://signalgo.azurewebsites.net/)
* [ASP.NET SignalR Hubs API Guide - Server (C#)](http://www.asp.net/signalr/overview/signalr-20/hubs-api/hubs-api-guide-server) 
* [Fuego](http://fuego.sourceforge.net/)

## Tech
* [SignalR](http://www.asp.net/signalr)
* [jGoBoard](http://jgoboard.com/)
* [SGF File Format](http://www.red-bean.com/sgf/) *'Smart Game Format'.*
