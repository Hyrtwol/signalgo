﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SignalGO.Test
{
    [TestFixture]
    public class MiscTests
    {
        [Test]
        public void SomeSizes()
        {
            Debug.Print("19*19={0}", 19*19);
            Assert.AreEqual(19*19, 361);
        }
    }
}
