using System;
using System.Web;
using Repertoire;

namespace SignalGO.Handlers
{
    internal static class GoHtmlExtensions
    {
        public static void WriteHeader(this HttpResponse response, string title)
        {
            response.WriteLine("<div id=\"header\" class=\"header\">");
            response.WriteLine(title);
            response.WriteLine("</div>");
        }

        public static void WriteFooter(this HttpResponse response)
        {
            response.WriteLine("<div id=\"footer\" class=\"footer\">");
            response.Write("{0}", typeof(HtmlHandler).Assembly.FullName);
            response.WriteLine("</div>");
        }

        public static void WritePanel(this HttpResponse response, string className, Action<HttpResponse> action)
        {
            response.WriteLine("<div class=\"{0}\">", className);
            action(response);
            response.WriteLine("</div><!-- {0} -->", className);
        }

        public static void WritePanelHeader(this HttpResponse response, string text)
        {
            response.WriteLine("<div class=\"panel-header\">{0}</div>", text);
        }
    }
}