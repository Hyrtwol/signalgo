﻿using System.Web;
using Repertoire;

namespace SignalGO.Handlers
{
    internal class HtmlHandler : IHttpHandler
    {
        private const string Title = "SignalR Go Game";

        public bool IsReusable { get { return true; } }

        public void ProcessRequest(HttpContext context)
        {
            //var request = context.Request;
            var response = context.Response;

            response.WriteDocBegin();
            response.WriteHeadBegin();
            response.WriteTitle(Title);
            response.WriteLink("~/jgo/jgoboard.css");
            //response.WriteLink("~/jgo/jgoboard_small.css");
            response.WriteLink("~/sgo.css");
            response.WriteHeadEnd();
            response.WriteBodyBegin();

            WriteContainer(response);

            response.WriteJavaScriptReference("~/Scripts/jquery-1.9.0.min.js");
            response.WriteJavaScriptReference("~/Scripts/jquery.signalR-2.1.0.min.js");
            response.WriteJavaScriptReference("~/jgo/jgoboard.js");
            response.WriteJavaScriptReference("~/signalr/hubs");
            response.WriteJavaScriptReference("~/sgo.js");

            response.WriteDocEnd();
        }

        private static void WriteContainer(HttpResponse response)
        {
            response.WriteLine("<div id=\"container\" class=\"container\">");
            response.WriteHeader(Title);
            response.WritePanel("content", ContainerContent);
            response.WriteFooter();
            response.WriteLine("</div><!-- container -->");
        }

        private static void ContainerContent(HttpResponse response)
        {
            response.WritePanel("panel player", PlayerPanel);
            response.WritePanel("panel moves", resp => WriteOutputList(resp, "moves"));
            response.WritePanel("game-area", WriteGoBoard);
            response.WritePanel("panel chatbox", ChatBox);
            response.WritePanel("panel discussion", resp => WriteOutputList(resp, "discussion"));
        }

        private static void PlayerPanel(HttpResponse response)
        {
            response.WritePanelHeader("Player");
            response.WriteDivBegin("panel-row");
            response.WriteLine("<input type=\"radio\" name=\"R1\" id=\"radioBlack\" value=\"1\"/>" +
                           " <img alt=\"Black\" class=\"auto-style1\" src=\"jgo/small/black.png\" />");
            response.WriteLine("<input type=\"radio\" name=\"R1\" id=\"radioWhite\" value=\"2\"/>" +
                           " <img alt=\"White\" class=\"auto-style1\" src=\"jgo/small/white.png\" />");
            response.WriteLine("<input type=\"radio\" name=\"R1\" id=\"radioClear\" value=\"0\"/>" +
                           " Clear");
            response.WriteDivEnd();
            response.WriteDivBegin("panel-row");
            response.WriteLine("<input type=\"button\" id=\"newBoard\" value=\"New Board\"/>");
            response.WriteDivEnd();
        }

        private static void WriteGoBoard(HttpResponse response)
        {
            response.WriteLine("<div class=\"jgo_board\" id=\"board\"></div>");
        }

        private static void ChatBox(HttpResponse response)
        {
            response.WritePanelHeader("Messages");
            response.WriteLine("<div>");
            response.WriteLine("<input type=\"hidden\" id=\"displayname\" />");
            response.WriteLine("<input type=\"text\" id=\"message\" size=\"30\" />");
            response.WriteLine("<input type=\"button\" id=\"sendmessage\" value=\"Send\" />");
            response.WriteLine("</div>");
        }

        private static void WriteOutputList(HttpResponse response, string name)
        {
            response.WriteLine("<div><ul id=\"{0}\" class=\"{0}\"></ul></div>", name);
        }
    }
}