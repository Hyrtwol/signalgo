﻿using System.Diagnostics;
using System.Threading.Tasks;
using GO;

namespace SignalGO.Hub
{
    public class ChatHub : Microsoft.AspNet.SignalR.Hub
    {
        public ChatHub()
        {
            //Groups.Add(Context.ConnectionId, groupName);

        }

        public override Task OnConnected()
        {
            // Add your own code here.
            // For example: in a chat application, record the association between
            // the current connection ID and user name, and mark the user as online.
            // After the code in this method completes, the client is informed that
            // the connection is established; for example, in a JavaScript client,
            // the start().done callback is executed.
            Debug.Print("OnConnected: ConnectionId={0}", Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            // Add your own code here.
            // For example: in a chat application, mark the user as offline, 
            // delete the association between the current connection id and user name.
            Debug.Print("OnConnected: ConnectionId={0}", Context.ConnectionId);
            return base.OnDisconnected();
        }

        public override Task OnReconnected()
        {
            // Add your own code here.
            // For example: in a chat application, you might have marked the
            // user as offline after a period of inactivity; in that case 
            // mark the user as online again.
            Debug.Print("OnReconnected: ConnectionId={0}", Context.ConnectionId);
            return base.OnReconnected();
        }

        public void Send(string name, string message)
        {
            Debug.Print("ConnectionId={0}", Context.ConnectionId);
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(name, message);
        }

        public void StonePlaced(string name, int x, int y, GoStone stone)
        {
            //Debug.Print("stonePlaced {0} {1} {2} {3}", name, x, y, stone);
            Clients.All.setStone(name, x, y, stone);
        }

        public void StonePlaced2(string name, string coord, GoStone stone)
        {
            Debug.Print("StonePlaced2 {0} {1} {2}", name, coord, stone);
            Clients.All.setStone2(name, coord, stone);
        }

        public void NewBoard(string name)
        {
            Debug.Print("NewBoard {0} {1}", Context.ConnectionId, name);
            // Call the broadcastCommand method to update clients.
            Clients.All.clearBoard(name);
        }

        //private void BroadcastCommand(string name, string command)
        //{
        //    Debug.Print("BroadcastCommand {0} {1} {2}", Context.ConnectionId, name, command);
        //    // Call the broadcastCommand method to update clients.
        //    Clients.All.broadcastCommand(name, command);
        //}
    }
}