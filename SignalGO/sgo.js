﻿
var board; // this will hold the JGOBoard object that is tied to the DOM table element
var chat;
var myRadios;

$(function () {
    // Declare a proxy to reference the hub.
    //$.connection('/signalr2');
    //$.connection.hub.url = '/signalr';

    chat = $.connection.chatHub;
    // Create a function that the hub can call to broadcast messages.
    //chat.client.broadcastMessage = function (name, message) {
    //    var encodedName = $('<div />').text(name).html();
    //    var encodedMsg = $('<div />').text(message).html();
    //    $('#discussion').append('<li><strong>' + encodedName
    //        + '</strong>:&nbsp;&nbsp;' + encodedMsg + '</li>');
    //};

    chat.client.broadcastMessage = gotMessage;
    //chat.client.broadcastCommand = gotCommand;
    chat.client.clearBoard = onClearBoard;
    chat.client.setStone = onSetStone;
    chat.client.setStone2 = onSetStone2;
    
    $("#radioBlack").prop("checked", true);
    myRadios = $("input[name=R1]");
    
    //$('#loginButton').click(function () {
    //    // Call the Send method on the hub. 
    //    chat.server.login($('#loginName').val());
    //});

    // Get the user name and store it to prepend to messages.
    $('#displayname').val(prompt('Enter your name:', ''));
    // Set initial focus to message input box.  
    $('#message').focus();
    // Start the connection.
    $.connection.hub.start().done(function () {
        $('#sendmessage').click(function () {
            // Call the Send method on the hub. 
            chat.server.send($('#displayname').val(), $('#message').val());
            // Clear text box and reset focus for next comment. 
            $('#message').val('').focus();
        });
    });
    
    $('#newBoard').click(function () {
        // Call the Send method on the hub.
        chat.server.newBoard($('#displayname').val());
    });

    board = jgo_generateBoard($("#board")); // generate the board
    board.click = boardClick; // listen to board clicks - JGOCoordinate parameter will be passed

    /* Text selection with mouse double click creates artifacts on Chrome at least */
    $("#board").attr('unselectable', 'on').css('-moz-user-select', 'none').each(function () {
        this.onselectstart = function () { return false; };
    });

});

function gotMessage(name, message) {
    // Html encode display name and message. 
    var encodedName = $('<div />').text(name).html();
    var encodedMsg = $('<div />').text(message).html();
    // Add the message to the page. 
    $('#discussion').append('<li>' + encodedName
        + ':&nbsp;&nbsp;' + encodedMsg + '</li>');
};
//function gotCommand(name, message) {
//    // Html encode display name and message. 
//    var encodedName = $('<div />').text(name).html();
//    var encodedMsg = $('<div />').text(message).html();
//    // Add the message to the page. 
//    $('#discussion').append('<li><strong>' + encodedName
//        + ':&nbsp;&nbsp;' + encodedMsg + '</strong></li>');
//};

/* jGoBoard calls the click handler when user clicks on a coordinate - parameter is JGOCoordinate object */
function boardClick(coord) {

    var stone = board.get(coord);

    var selectedRadio = myRadios.filter(":checked");
    var playerStone = selectedRadio.val();

    if (stone != playerStone)
        //chat.server.stonePlaced($('#displayname').val(), coord.i, coord.j, playerStone);
        chat.server.stonePlaced2($('#displayname').val(), coord.toString(), playerStone);

    //chat.server.send($('#displayname').val(), coord+ " -> " + stone);
    //chat.server.stonePlaced($('#displayname').val(), coord, stone);
}

function onSetStone(name, x, y, stone) {
    var coord = new JGOCoordinate(x, y);
    //var stoneBefore = board.get(coord);
    var encodedName = $('<div />').text(name).html();
    var encodedMsg = $('<div />').text(coord).html();
    // Add the message to the page. 
    $('#moves').append('<li class="move' + stone + '"><span class="move">' + encodedName + '&nbsp;&nbsp;' + encodedMsg + '</span></li>');
    board.set(coord, stone);
}

function onSetStone2(name, newcoord, stone) {
    var coord = new JGOCoordinate(newcoord);
    //var stoneBefore = board.get(coord);
    var encodedName = $('<div />').text(name).html();
    var encodedMsg = $('<div />').text(coord).html();
    // Add the message to the page. 
    $('#moves').append('<li class="move' + stone + '"><span class="move">' + encodedName + '&nbsp;&nbsp;' + encodedMsg + '</span></li>');
    board.set(coord, stone);
}

function onClearBoard(name) {
    $('#moves').empty();
    board.clear();
    gotMessage(name, 'New board started');
}
