﻿using System;
using GO;

namespace SignalGo.Fuego
{
    public class BroadcastMessageEventArgs : EventArgs
    {
        public readonly string Name;
        public readonly string Message;

        public BroadcastMessageEventArgs(string name, string message)
        {
            Name = name;
            Message = message;
        }
    }

    public class SetStoneEventArgs : EventArgs
    {
        public readonly string Name;
        public readonly string Coord;
        public readonly GoStone Stone;

        public SetStoneEventArgs(string name, string coord, GoStone stone)
        {
            Name = name;
            Coord = coord;
            Stone = stone;
        }
    }

    public class ClearBoardEventArgs : EventArgs
    {
        public readonly string Name;

        public ClearBoardEventArgs(string name)
        {
            Name = name;
        }
    }
}