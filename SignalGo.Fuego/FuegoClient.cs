﻿using System;
using GO;

namespace SignalGo.Fuego
{
    internal class FuegoClient : IDisposable
    {
        private HubClient _hub;
        private FuegoProcess _fuego;

        public FuegoClient(string url)
        {
            _hub = new HubClient(url);
            _hub.BroadcastMessage += BroadcastMessage;
            _hub.ClearBoard += ClearBoard;
            _hub.SetStone += SetStone;

            _fuego = new FuegoProcess(Callback);
        }

        private void ClearBoard(object sender, ClearBoardEventArgs e)
        {
            Console.WriteLine("ClearBoard {0}", e.Name);
            _fuego.SendCommand("clear_board");
        }

        private void SetStone(object sender, SetStoneEventArgs e)
        {
            Console.WriteLine("OnSetStone2 {0} {1} {2}", e.Name, e.Coord, e.Stone);
            if (e.Stone == GoStone.Black)
            {
                var cmd1 = string.Format("play {0} {1}", e.Stone, e.Coord);
                Console.WriteLine(cmd1);
                _fuego.SendCommand(cmd1);

                var cmd2 = string.Format("genmove {0}", GoStone.White);
                Console.WriteLine(cmd2);
                _fuego.SendCommand(cmd2);
            }
        }

        private void BroadcastMessage(object sender, BroadcastMessageEventArgs e)
        {
            Console.WriteLine("{0}: {1}", e.Name, e.Message);
        }

        public void Dispose()
        {
            if (_hub != null)
            {
                _hub.Dispose();
                _hub = null;
            }
            if (_fuego != null)
            {
                _fuego.Stop();
                _fuego = null;
            }
        }

        public void Start()
        {
            _hub.Start();
            _fuego.Start();
        }

        private void Callback(string coord)
        {
            Console.WriteLine("Callback {0}", coord);
            _hub.StonePlaced("Robert", coord, GoStone.White);
        }

        public void Stop()
        {
            _hub.Stop();
            _fuego.Stop();
        }

        public void SendCommand(string command)
        {
            _hub.Send("Robert", command);
        }
    }
}