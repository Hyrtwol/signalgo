﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SignalGo.Fuego
{
    public class FuegoProcess
    {
        private readonly Action<string> _callback;
        public const string FuegoPath = @"C:\Program Files (x86)\Fuego\";
        public const string FuegoExe = "fuego.exe";

        private readonly CancellationTokenSource _cts;
        private Process _process;
        private readonly string _fuegoFullPath;
        private readonly string _fuegoExeFullPath;

        public FuegoProcess(Action<string> callback)
        {
            _callback = callback;
            _fuegoFullPath = Path.GetFullPath(FuegoPath);
            _fuegoExeFullPath = Path.Combine(_fuegoFullPath, FuegoExe);
            _cts = new CancellationTokenSource();
            _process = null;
        }

        public void Start()
        {
            var arguments = Environment.GetCommandLineArgs();
            if (arguments.Length > 2) Exit("Too many arguments");

            Console.WriteLine("Starting...");

            if (!File.Exists(_fuegoExeFullPath)) Exit("Couldn't find " + _fuegoExeFullPath);

            _process = StartFuego();
            if (_process.Start())
            {
                Console.WriteLine("Fuego process started...");

                var token = _cts.Token;
                Task.Factory.StartNew(() =>
                {
                    string line;
                    while (!token.IsCancellationRequested && (line = _process.StandardOutput.ReadLine()) != null)
                    {
                        if (_callback != null && line.StartsWith("="))
                        {
                            var coord = line.Substring(1).Trim();
                            if (!string.IsNullOrEmpty(coord))
                            {
                                _callback(coord);
                            }
                        }
                        Console.WriteLine(line);
                    }

                }, token);

            }
            else Exit("Failed to start Fuego process");
        }

        public void Stop()
        {
            Console.WriteLine("Sending Shutdown...");
            SendCommand("quit");

            //StopFuego();

            Console.WriteLine("Waiting for exit...");
            if (!_process.WaitForExit(5000))
            {
                Console.WriteLine("Killing redis process");
                _process.Kill();
            }
            Console.WriteLine("Fuego process exited. ExitCode=" + _process.ExitCode);
            _process.Dispose();
            _process = null;

            _cts.Cancel();
        }

        private Process StartFuego()
        {
            var startInfo = new ProcessStartInfo(_fuegoExeFullPath);
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardInput = true;
            startInfo.WorkingDirectory = _fuegoFullPath;
            return new Process {StartInfo = startInfo};
        }

        private void Exit(string message)
        {
            if (Environment.UserInteractive)
            {
                Console.WriteLine(message);
                //Environment.Exit(-1);
            }
            //else
            {
                //File.WriteAllText(Path.Combine(_path, "error.txt"), message);
                throw new ApplicationException(message);
            }
        }

        public void SendCommand(params string[] commands)
        {
            if (_process == null) return;
            foreach (var command in commands)
            {
                _process.StandardInput.WriteLine(command);
            }
        }
    }
}
