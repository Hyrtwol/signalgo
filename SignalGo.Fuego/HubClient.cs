﻿using System;
using System.Diagnostics;
using GO;
using Microsoft.AspNet.SignalR.Client;

namespace SignalGo.Fuego
{
    public class HubClient : IDisposable
    {
        private readonly HubConnection _hubConnection;
        private readonly IHubProxy _serverHub;
        //private Subscription _broadcastMessage;

        public event EventHandler<BroadcastMessageEventArgs> BroadcastMessage;
        public event EventHandler<SetStoneEventArgs> SetStone;
        public event EventHandler<ClearBoardEventArgs> ClearBoard;

        public HubClient(string url)
        {
            _hubConnection = new HubConnection(url);
            _serverHub = _hubConnection.CreateHubProxy("ChatHub");
            //_broadcastMessage=_serverHub.Subscribe("broadcastMessage");
            _serverHub.On("broadcastMessage", (Action<string, string>)OnBroadcastMessage);
            _serverHub.On("setStone2", (Action<string, string, GoStone>)OnSetStone);
            _serverHub.On("clearBoard", (Action<string>)OnClearBoard);
        }

        private void OnClearBoard(string name)
        {
            var handler = ClearBoard;
            if (handler == null) return;
            handler(this, new ClearBoardEventArgs(name));
        }

        private void OnSetStone(string name, string coord, GoStone stone)
        {
            var handler = SetStone;
            if (handler == null) return;
            handler(this, new SetStoneEventArgs(name, coord, stone));
        }

        protected virtual void OnBroadcastMessage(string name, string message)
        {
            var handler = BroadcastMessage;
            if (handler == null) return;
            handler(this, new BroadcastMessageEventArgs(name, message));
        }

        public void Dispose()
        {
            _hubConnection.Dispose();
        }

        public void Start()
        {
            _hubConnection.Start().Wait();
        }

        public void Stop()
        {
            _hubConnection.Stop();
        }

        public void Send(string name, string message)
        {
            _serverHub.Invoke("Send", name, message).Wait();
        }

        public void StonePlaced(string name, string coord, GoStone stone)
        {
            _serverHub.Invoke("StonePlaced2", name, coord, stone).Wait();
        }
    }
}