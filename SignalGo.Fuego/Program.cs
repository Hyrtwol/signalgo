﻿using System;

namespace SignalGo.Fuego
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            //const string url = "http://localhost:52548/";
            const string url = "http://signalgo.azurewebsites.net/";

            using (var fuegoClient = new FuegoClient(url))
            {
                Console.WriteLine("Starting");
                fuegoClient.Start();
                Console.WriteLine("Running");

                for (var line = Console.ReadLine(); !string.IsNullOrEmpty(line); line = Console.ReadLine())
                {
                    fuegoClient.SendCommand(line);
                }

                Console.WriteLine("Stopping");
                fuegoClient.Stop();
            }

            Console.WriteLine("Done.");
            Console.ReadLine();
        }
    }
}
